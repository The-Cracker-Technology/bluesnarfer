CC = gcc
LD = ld
LDFLAGS = -lbluetooth
SRC = src
INC = -Iinclude

all: bluesnarfer

bluesnarfer:
	$(CC) $(SRC)/bluesnarfer.c $(INC) $(CFLAGS) $(LDFLAGS) -o bluesnarfer
